#!/usr/bin/env python
"""  read raw input data
sanitize, split
write data as YAML files, also the k-fold split file
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#exit(2) # block  overwrite

from Plotter_SecMet import Plotter_SecMet

import argparse,os
def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--dataPath", default='data',
                        help="path to input/output")
    parser.add_argument("--outPath", default='out',
                        help="output path for plots")

    parser.add_argument("-n", "--events", type=int, default=1000,
                        help="max events for role, use 0 for all")
 
    parser.add_argument( "-X","--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjVersion='1'
    args.prjName='secmet'+args.prjVersion # core name used everywhere

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    args.arrIdx=0 # for plotter, not needed here
    assert os.path.isdir(args.outPath) # check it now to avoid lost time
    assert os.path.isdir(args.dataPath) # check it now to avoid lost time
    return args

# raw data path from Dan
commPath='/global/dna/projectdirs/secmet/secmet/mlproject/data2018-10/'
rawFN={}
rawFN['priMet']=commPath+'priMetaGenes.faa'
rawFN['secMet']='raw_feb27/MIBIG.clean.first6593.faa'

from Util_SecMet import read_proteins_fasta, split_into_segments, save_species_split, write_yaml
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
gra=Plotter_SecMet(args )

aminoacidL='ACDEFGHIKLMNPQRSTVWXY'
numSeg=9
seqLenClip=[100,1500]
roleL=['priMet','secMet']
metaD={'numSeg':numSeg,'aminoL':aminoacidL,'rawDataPath':commPath}

eveCnt={}
cntD={}
#for role in roleL:
for role in roleL:
    print('ingest role=',role)
    protL,cnt=read_proteins_fasta(rawFN[role],aminoacidL, seqLenClip, role, args.events, maxRec=int(1e6))
    gra.plot_protein_len(protL,'input:'+role,5)
    segD=split_into_segments(protL,numSeg)
    save_species_split(segD,role,args)
    eveCnt[role]=len(segD[0])
    cntD[role]=cnt
    #break
    
metaF=args.dataPath+'/'+args.prjName+'.meta.yml'
print('assemble meta data :',metaF)


#print('eveCnt',eveCnt)
metaD['evePerSeg']=eveCnt
metaD['cnt']=cntD
metaD['padChar']='0'
metaD['seqLenClip']=seqLenClip
metaD['roleL']=roleL
write_yaml(metaD,metaF)

gra.display_all(args,'form')

