import os, time
#import warnings
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
#warnings.filterwarnings("ignore") #Hide messy Numpy warnings
from sklearn.metrics import roc_curve, auc, confusion_matrix

start = time.time()
from Oracle_SecMet import Oracle_SecMet

from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint ,ReduceLROnPlateau, Callback 
from tensorflow.keras.layers import Dense, Dropout, Input, concatenate,Conv1D,LeakyReLU,MaxPool1D,Flatten
from tensorflow.keras.models import Model, load_model
import tensorflow.keras.backend as K
import tensorflow as tf
from tensorflow.contrib.training import HParams

import random
import numpy as np
from pprint import pprint
from ruamel.yaml import YAML as yaml

from Util_SecMet import  read_yaml ,write_yaml#, ,normalize_features

print('deep-secmet imported, TF.ver=%s, elaT=%.1f sec'%(tf.__version__,(time.time() - start)))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]   
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        self.hir.append(float(lr))

# - - - - - - - - - - - - - - - - - - - - - - 
class YParams(HParams):
    """ Yaml file parser derivative of HParams """
    def __init__(self, yaml_fn, config_name):
        super(YParams, self).__init__()
        with open(yaml_fn) as yamlfile:
            for key, val in yaml().load(yamlfile)[config_name].items():
                print('hpar',key,':', val)
                self.add_hparam(key, val)


#............................
#............................
#............................
class Deep_SecMet(Oracle_SecMet):

    def __init__(self,args):
        Oracle_SecMet.__init__(self,args)
        self.name=args.prjName
        print(self.__class__.__name__,', prj:',self.name)

        self.kfoldOffset=args.kfoldOffset
        self.events=args.events
        
        self.numSegm=self.inpMetaD['numSeg']
        self.dataPath=args.dataPath
        self.train_hirD={'acc': [],'loss': [],'lr': [],'val_acc': [],'val_loss': []}
        
        print('globFeatureL %d fixed order:'%len(self.globFeatureL),self.globFeatureL)
        # . . . load hyperparameters
        yaml_cnfg='hpar_'+args.prjName+'.yaml'
        self.hparams=YParams(os.path.abspath(yaml_cnfg),args.modelDesign )
        #self.hparams.add_hparam('modelHook', self)

    #............................
    def prep_labeled_input(self,role,dom):  #dom==domain
        max_eve=self.events/2  # half of data are primet half are secmet
        print('\nprep_labeled_input role=:',role, ' max=',max_eve)
        start=time.time()
        if self.verb>1:
            print(' found %d split-segments:'%nSeg,list(inpD.keys()),', kfoldOffset=', self.kfoldOffset)
            print('load_labeled_input_yaml dom=',dom,'kfoldOffset=', self.kfoldOffset)

        numSegm=self.numSegm-1
        numTrainSeg=numSegm-1

        assert numTrainSeg>0  # makes no sense to split to train/eval/test
        n0=self.kfoldOffset

        if dom=='val':
            jL=[(n0+numTrainSeg)%numSegm] # one element 

        if dom=='test':
            jL=[numSegm] # one element, fixed,                 

        if dom=='train':
            jL=[ (n0+j)%numSegm for j in range(numTrainSeg)]

        # agregate all segments, if needed
        wrkL=[]            
        for k in jL: 
            inpF=self.dataPath+'/'+self.name+'.%s.seg%d.yml'%(role,k)
            inpD=read_yaml(inpF,verb=0)
            #print('read ',inpF)
            wrkL+=inpD
            if max_eve>0:
                if len(wrkL)>max_eve : 
                    print('after k=',k,' got enough events')
                    break 

        assert len(wrkL)>0

        # generate index for data to match requested sample size
        numSamples=int(self.events/2)
        if dom!='train' : numSamples=int(numSamples/8)
        assert numSamples>9
        inpSize=len(wrkL)
        doReplace=inpSize<numSamples
        idxL=np.random.choice(inpSize,numSamples,replace=doReplace)
        
        print('  role:',role,'segL:',jL,' dom:',dom,', numSpec:',len(wrkL),', kfoldOffset=', self.kfoldOffset,' overSample=%.3f'%(numSamples/inpSize))
 

        if dom not in self.amino_data:
            self.amino_data[dom]={}
        self.amino_data[dom][role]=[numSamples,idxL, wrkL]

        print('prep:',dom,role,' completed, elaT=%.1f sec'%(time.time() - start),', gotSamples=',numSamples)

        
  
    #............................
    def build_training_data(self,dom):
        start = time.time()
        seq_len=self.seqLenCut
        num_bases=len(self.basesL)
        num_features=len(self.globFeatureL)
        print('build_training_data: seq_len=',seq_len,' num_bases=',num_bases,' num_globfeatures=',num_features) 

        num0=0; offIdx={}
        for role in self.roleL: 
            numSamples,idxL, wrkL=self.amino_data[dom][role]

            # ..... this was alwyas confusing part
            if num0==0: # this will be score=0 class (aka negative data)
                offIdx[role]=0 
                num0=numSamples
            else:   # this will be score=1 class (aka positive data)
                offIdx[role]=num0
                num1=numSamples

        #print('qq offIdx:',offIdx,num0,num1,'dims:',seq_len,num_bases,num_features)
        # clever list-->numpy conversion, Thorsten's idea
        XhotAll=np.zeros([num0+num1,seq_len,num_bases],dtype=np.float32)
        XfloatAll=np.zeros([num0+num1,num_features],dtype=np.float32)
        AuxAll=['aa' for x in range(num0+num1)  ]   # put here aux variables as list

        YAll=np.zeros([num0+num1],dtype=np.float32)
        YAll[num0:]=np.ones(num1) # before randomization all plasmids are in 2nd half of array

        #print(dom,'wq0',XhotAll.shape,XfloatAll.shape)
        
        cnt={}
        for role in self.roleL: 
            numSamples,idxL, wrkL=self.amino_data[dom][role]
            ieve=offIdx[role]
            cnt[role]={'short':0,'long':0,'any':0}
            for i in idxL:
                oneL=wrkL[i]
                cnt[role]['any']+=1
                strTag=self.add_prot_to_1hot(XhotAll,XfloatAll,AuxAll, ieve,oneL)                
                ieve+=1
                cnt[role][strTag]+=1             

        print('build_training_data for dom:',dom,'Xs,Xf,Y:',XhotAll.shape,XfloatAll.shape,YAll.shape,'SNR=%.3f'%(num1/num0),'done')
        print( ' dom=%s elaT=%.1f sec'%(dom,time.time() - start),'  cnt:',cnt)
        self.data[dom]=[XhotAll,XfloatAll,YAll,AuxAll]

        return
        # test print of data
        for i in range(YAll.shape[0]):
            Xhot=XhotAll[i]
            Xflo=XfloatAll[i]
            y=YAll[i]
            print('i=',i,Xflo,y,Xhot.shape)
            print(Xhot)
            ok44


    #............................
    def build_compile_model(self,args):
        XA,XB,Y,AUX=self.data['train']
        
        shA=XA.shape
        shB=XB.shape
        xA = Input(shape=(shA[1],shA[2]), name='seq_%d_x_%d'%(shA[1],shA[2]))
        xB = Input(shape=(shB[1],), name='globF_%d'%(shB[1]))
        print('build_model inpA:',xA.get_shape(),'  inpB:',xB.get_shape())
        

        pprint(self.hparams)

        # clumsy way to insert if  LeakyReLU requested
        hiddenAct=self.hparams.hiddenAct
        hidAct=hiddenAct 
        if hiddenAct=='leakyReLU':
            hidAct='linear'
            print('insert',hiddenAct)

        kernel_sizes = self.hparams.conv_kernels
        filters = self.hparams.conv_filters
        stride = self.hparams.conv_stride
        pool=self.hparams.conv_pool
        padd=self.hparams.conv_padding
        fcDims=self.hparams.fc_dims

        h=xA
        # .....  CNN-1D layers:
        for i, (_kern, _filt) in enumerate(zip(kernel_sizes, filters)):
            _name = 'conv1d_%i'%i
            print(i,'CNN add layer:',_kern, _filt,_name)
            h= Conv1D(_filt,_kern,strides=stride, padding=padd,name='cnb%d_d%d_k%d'%(i,_filt,_kern), activation=hidAct)(h)
            if hiddenAct=='leakyReLU':
                h =LeakyReLU(name='act%d'%(i))(h)
            if self.hparams.conv_dropFrac>0.05:
                h = Dropout(self.hparams.conv_dropFrac,name='cndr%d'%i)(h)
            h= MaxPool1D(pool_size=pool, name='pool_%d'%(i))(h)

        netA=Flatten(name='to_1d')(h)
        h = concatenate([netA,xB],name='add_AB')
         
        # .... FC  layers
        numFC=len(fcDims)
        for i in range(numFC):
            dim = fcDims[i]
            h = Dense(dim,activation=hidAct,name='enc%d'%i)(h)
            if hiddenAct=='leakyReLU':
                h =LeakyReLU(name='acc%d'%(i))(h)
            if self.hparams.fc_dropFrac>0.05:
                h = Dropout(self.hparams.fc_dropFrac,name='fcdr%d'%i)(h)

        out_dim=1 # this is binary classfire
        y= Dense(out_dim, activation='sigmoid',name='out_%d'%out_dim)(h)

        model = Model(inputs=[xA,xB], outputs=y)
        self.model=model
        self.compile_model()

    #............................
    def load_compile_1model(self,path='.'):  # for re-training
        try:
            del self.model
            print('delet old model')
        except:
            a=1
        start = time.time()
        inpF5m=path+'/'+self.name+'.model.h5'
        print('load 1model and weights  from',inpF5m,'  ... ')
        self.model=load_model(inpF5m) # creates mode from HDF5
        #self.model.summary()
        print(' model loaded, elaT=%.1f sec'%(time.time() - start))
        self.compile_model()



    #............................
    def compile_model(self):
        """ https://machinelearningmastery.com/save-load-keras-deep-learning-models/
        It is important to compile the loaded model before it is used. 
        This is so that predictions made using the model can use 
        the appropriate efficient computation from the Keras backend.
        """
        print(' (re)Compile model')
        start = time.time()
        self.model.compile(loss='binary_crossentropy', optimizer=self.hparams.optimizer, metrics=['accuracy'])
        self.model.summary() # will print
        print('model (re)compiled elaT=%.1f sec'%(time.time() - start))
        

    #............................
    def train_model(self,args):
        XA,XB,Y,AUX=self.data['train']
        XA_val,XB_val,Y_val,AUX_val=self.data['val']

        epochs=args.epochs
 
        if args.verb==0:
            print('train for epochs:',args.epochs)
     
        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if args.earlyStopPatience>0:
            earlyStop=EarlyStopping(monitor='val_loss', patience=args.earlyStopPatience, verbose=1, min_delta=1.e-5, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',args.earlyStopPatience)

        if args.checkPtOn:
            outF5w=self.outPath+'/'+self.name+'.weights_best.h5'
            chkPer=1
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=chkPer)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint, period=',chkPer)

        if args.reduceLearnPatince>0:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=args.reduceLearnPatince, min_lr=0.0, verbose=1,min_delta=0.003)
            callbacks_list.append(redu_lr)
            print('enabled ReduceLROnPlateau, patience=',args.reduceLearnPatince)
        
        # final check of positive/negative label balance
        sumT=sum(Y); snrT= sumT/(Y.shape[0]-sumT)
        sumV=sum(Y_val); snrV= sumV/(Y_val.shape[0]-sumV)
        print('\nTrain_model X:',XA.shape,' epochs=',epochs,' batch=',args.batch_size,', data SNR(train)=%.4f, SNR(valid)=%.4f'%(snrT,snrV))

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if args.verb==2: fitVerb=1 
        if args.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        startTm = time.time()
        hir=self.model.fit([XA,XB],Y, callbacks=callbacks_list,
                 validation_data=([XA_val,XB_val],Y_val),  shuffle=True,
                 batch_size=args.batch_size, epochs=epochs, 
                 verbose=fitVerb)
        fitTime=time.time() - start

        hir=hir.history
        for obs in hir:
            rec=[ float(x) for x in hir[obs] ]
            self.train_hirD[obs].extend(rec)
        
        # this is a hack, 'lr' is returned by fit only when --reduceLr is used
        if 'lr' not in  hir: 
            self.train_hirD['lr'].extend(lrCb.hir)

        loss=self.train_hirD['val_loss'][-1]
        acc=self.train_hirD['val_acc'][-1]
        acc0=self.train_hirD['val_acc'][0]
        nEpoch=len(self.train_hirD['val_loss'])
      
        print('\n Validation Acc:%.3f -->%.3f'%(acc0,acc), ', end-loss:%.3f'%loss,' %d epochs'%(nEpoch),', fit time=%.1f min'%(fitTime/60.))
        self.train_sec=fitTime
        self.acc=acc


    #............................
    def save_model(self):
        outF=self.outPath+'/'+self.name+'.model.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('  closed  hdf5:',outF,' size=%.2f MB'%xx)
 
    #............................
    def load_weights_4_retrain(self,path='.'):
        start = time.time()
        inpF5m=path+'/'+self.name+'.weights_best.h5'
        print('load  weights_4_retrain  from',inpF5m,end='... ')
        self.model.load_weights(inpF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))


    #............................
    def save_training_history(self) :
        outD=self.train_hirD
        outF=self.outPath+'/'+self.name+'.history.yml'
        write_yaml(outD,outF)


   #............................
    def model_predict_domain(self,dom):
        start = time.time()
        kf='' # here only one kflod is expected to be loaded
        (XA,XB,y_true)=self.data[dom]
        kf='' # here only one kflod is expected to be loaded
        print('model_predict_domain :%s   Y-size=%d'%(dom,len(y_true)))
        y_score = self.model[kf].predict([XA,XB]) 

        fpr, tpr, _ = roc_curve(y_true, y_score)
        roc_auc = float(auc(fpr, tpr))

        # this is wastefull - 2nd time computes score inside 'evaluate(..)'
        score = self.model[kf].evaluate([XA,XB],y_true, verbose=0)
        #print(' loss:', score[0])
        #print('accuracy:', score[1])
        loss= float(score[0])
        accuracy=float(score[1])

        nAll=y_true.shape[0]
        L1=int(y_true.sum())
        L0=nAll - L1

        print('  segment done, loss=%.3f, acc=%.3f, AUC=%.3f   elaT=%.1f sec'%(loss,accuracy,roc_auc,time.time() - start))

        sumD={'accuracy':accuracy,'AUC':roc_auc,'loss':loss,'nNeg':L0,'nPos':L1}

        return sumD


