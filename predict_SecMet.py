#!/usr/bin/env python
""" 
 classify a proteins
 uses a series of pre trained models (kfolds), 
 run prediction with each model, then average the score
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_SecMet import Plotter_SecMet
from Oracle_SecMet import Oracle_SecMet
from Util_SecMet import write_yaml, read_yaml , dump_ana_details, floatL2histo

import yaml
import math
from pprint import pprint

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description=' SecMet Oracle  predicts',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2,3],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--dataPath",
                        default='data20k',help="path to input")

    parser.add_argument('-y',"--inputSegment", type=str,
                        default='secMet.seg8',help="which data file to process")

    parser.add_argument("-k","--kModelList",nargs="+",
                        default=[''],
                        help=" blank separated list of kfold IDs, takes also n1-n2")

    parser.add_argument("--outPath",
                        default='outPR',help="output path for plots/output")
 
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="max number of preteins, 0=all")
 
    parser.add_argument("--seedModel",
                        #default='/global/cscratch1/sd/balewski/secMet1a/16814178-',
                        default='out/',
                        help="trained model and weights")

    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjVersion='1'
    args.prjName='secmet'+args.prjVersion # core name used everywhere

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
     
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()


inpF=args.dataPath+'/'+args.prjName+'.%s.yml'%(args.inputSegment)
protL=read_yaml(inpF)

score_thr=0.80
if args.events>0:
    max_pred=args.events
else:
    max_pred=len(protL)

print('M: inp:',args.inputSegment,' num prot:', len(protL),' score_thr=',score_thr,' max_pred=',max_pred)

ora=Oracle_SecMet(args)
ora.load_Kmodels(path=args.seedModel,kL=args.kModelList)
cnt={'inp':0,'short':0,'long':0,'AMBIG':0}
for x in ora.roleL: cnt[x]=0
avrL=[]
scoreD={}
outD={'data_info':{'inpSeg':args.inputSegment}, 'model_info': ora.info}

for  oneL in protL:
    protN=oneL[0]
    cnt['inp']+=1
    if cnt['inp']>= max_pred: break
    XhotA,XfloatA,AuxAll,strTag=ora.build_data_one(oneL)
    cnt[strTag]+=1              
    if args.verb>2:
        dump_ana_details(oneL,XhotA,XfloatA)

    avr_str,rec1=ora.classify_one_protein(XhotA,XfloatA,verb=args.verb-1)
    avrL.append(rec1['avr'])
    #print('M:score protName:',protN,avr_str,rec1)

    roleIdx=-1
    if rec1['avr']>score_thr+rec1['err']*2:
        roleIdx=1
    elif rec1['avr']<score_thr-rec1['err']*2:
        roleIdx=1

    rolePred= 'AMBIG'
    if roleIdx>=0:
        rolePred=ora.roleL[roleIdx]
    cnt[rolePred]+=1
    scoreD[protN]=rec1

outD['ora_inp']={'seqLen':XhotA.shape[1],'floatLen':XfloatA.shape[1]}
if args.verb>1:
    print('out scoreD='); pprint(scoreD)
    print('avrL',avrL)

histD={'x0':0,'x1':1,'nb':21}
floatL2histo(avrL,histD)
print('out histD='); pprint(histD)
print('out outD='); pprint(outD)

outD['scores']=scoreD
outD['hist']=histD
write_yaml(outD,args.outPath+'/%s.pred.%s.yml'%(args.prjName,args.inputSegment))

print('M: endCnt:',cnt)

# make plot of all scores
gra=Plotter_SecMet(args )
y1=0
if 'sec' in args.inputSegment: y1=1

YtrueL=[y1]*len(avrL)
gra.plot_labeled_scores(YtrueL, avrL,ora.roleL,'proteins: '+args.inputSegment,score_thr=score_thr,figId=21)
gra.display_all(args,'pred')
print('prediction has been completed')
