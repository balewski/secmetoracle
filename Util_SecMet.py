# small operations w/o dependencies
# keep double indent - in case you want to make a class of it

import os, time
import yaml
from yaml import CLoader, CDumper
from random import shuffle
import numpy as np

#...!...!....................
def read_proteins_fasta(fName,aminoacidL, seqLenClip, role, askRec, maxRec=5000):
        # the askRec & maxRec  allow to read more data and randomly donwsample
        aminoacidSet=set(aminoacidL)
        nSkip=0
        #X if 'priMeta' in fName: nSkip=1000000
        maxRec=max(askRec,maxRec)
        print('read from ',fName,' seqLenClip=',seqLenClip,' maxRec=',maxRec,nSkip)

        seqLenLo, seqLenHi=seqLenClip
        protDB=['none%d'%x for x in range(maxRec)  ]   # it should speed up
        fp = open(fName, 'r')
        protN=''
        cnt={'lo':0,'acc':0,'hi':0,'any':0,'skip':0}
        for line in fp:
            line = line.rstrip()
            if line[0]=='>':#  header: Name|Species|association
                cnt['any']+=1

                #print(line)
                if cnt['acc']>= maxRec: break # enough data was collected
                if cnt['any'] %50000==0:
                    print('add ',protN,cnt)
                if len(protN) >0: #archive last protein
                    seqLen=len(seq)
                    if seqLen <=seqLenLo:
                            cnt['lo']+=1
                    elif  nSkip>0 and  cnt['any']<nSkip : 
                           cnt['skip']+=1 
                    else: # accept those sequences
                        if seqLen >seqLenHi: # clip too long sequences
                            cnt['hi']+=1
                            seq=seq[:seqLenHi]
                        fastaIdx=cnt['any']
                        protDB[cnt['acc']]= [ protN, seqLen, fastaIdx, seq]
                        cnt['acc']+=1                            
                       
                    protN='none1'; seq='none2'

                if role=='priMet': # Fasta Header1 speciffic
                        assert 'from' in line                
                        line2=line[1:].replace(' from ',':')
                        #print('line2=',line2)
                        protN=line2
                elif role=='secMet': # Fasta Header2 speciffic
                        assert '|' in line                
                        line2=line[1:].split('|')
                        #print('line2=',line2)
                        protN=line2[0]+':'+line2[-1]
                else:
                        bad_role1

                seq=''
            else:    # - - - - amino acid codes  are valid
                #print('sss',set(line)-aminoacidSet)
                #print(line)
                if 'B' in line: line=line.replace('B','') # on Dan's request, 2019-02-28
                assert aminoacidSet.issuperset(set(line))
                assert len(protN)>0
                seq+=  line
        fp.close()

        protDB=protDB[:cnt['acc']] # clip  not used list elements
        askRec=min(askRec,len(protDB))
        print('  read proteins' ,cnt,' out list len=',len(protDB), 'askRec=',askRec)
        shuffle(protDB) # in place
        return protDB[:askRec],cnt


#...!...!....................
def split_into_segments(dataL,numSegm):
        nTot=len(dataL)
        segCnt=int(nTot/numSegm)
        print('split %d data into %d segments, %d data per segment'%(nTot,numSegm,segCnt))

        outD={}
        for seg in range(numSegm):
            i1=seg*segCnt
            i2=i1+segCnt
            #print('sss',seg,i1,i2)
            outD[seg]=dataL[i1:i2]
        return outD

#...!...!....................
def save_species_split(inpD,role,args) :
        #repacks data for saving
        roleL=list(inpD.keys())
        numSeg=len(inpD)

        for seg in  inpD:
            outF=args.dataPath+'/'+args.prjName+'.%s.seg%d.yml'%(role,seg)
            #print('save segment %d as yaml:'%seg,outF)
            ymlf = open(outF, 'w')
            yaml.dump(inpD[seg], ymlf, Dumper=CDumper)
            ymlf.close()
            xx=os.path.getsize(outF)/1048576
            print('  closed  yaml:',outF,' size=%.2f MB'%xx)

#...!...!....................
def read_yaml(ymlFn,verb=1):
        if verb:  print('  read  yaml:',ymlFn)
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=CLoader)
        ymlFd.close()
        if verb: print(' done, size=%d'%len(bulk))
        return bulk

#...!...!....................
def write_yaml(outD,outF) :
        print('save yaml:',outF)
        ymlf = open(outF, 'w')
        yaml.dump(outD, ymlf, Dumper=yaml.CDumper)
        ymlf.close()


#...!...!....................
def dump_ana_details(oneL,Xhot,Xfloat):
        protN,seqLen,fastaIdx,seqStr=oneL
        print('\n======== dump_ana_details name=',protN,' seqLen:',seqLen,len(seqStr),' inp shapes:',Xhot.shape,Xfloat.shape)

        k=30
        print('\n sample of input[:%d] Xloat:'%k,Xfloat[0])
        for i in range(k):
            print( i,seqStr[i],' oneHot:',Xhot[0][i])


#...!...!....................
def floatL2histo(valL,info):
        x0=info['x0']
        x1=info['x1']
        nb=info['nb']

        bins = np.linspace(x0,x1,nb)
        hist_np,_= np.histogram(valL, bins)
        hist=[ float(x) for x in hist_np]
        info['hist']=hist

