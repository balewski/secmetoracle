import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()

from tensorflow.keras.models import Model, load_model
from sklearn.metrics import roc_curve, auc,confusion_matrix
from scipy.stats import skew

import datetime
from pprint import pprint
import numpy as np
import yaml

from Util_SecMet import read_yaml 
print('ora-secmet imported , elaT=%.1f sec'%((time.time() - start)))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Oracle_SecMet(object):
    def __init__(self,args):
        self.name=args.prjName
        self.verb=args.verb
        print(self.__class__.__name__,', prj:',self.name)
 
        metaF=args.dataPath+'/'+args.prjName+'.meta.yml'
        metaD=read_yaml(metaF)
        print('read meta data:',metaD)
        self.outPath=args.outPath             

        self.padChar=metaD['padChar']
        self.basesL=metaD['aminoL']
        self.seqLenCut=1200 # sample size in amino acids
        self.globFeatureL=['len_sequence']
        self.roleL=metaD['roleL']
        self.runDate=datetime.datetime.now()
        self.inpMetaD=metaD
        assert os.path.isdir(args.outPath) # check it now to avoid lost time
        assert os.path.isdir(args.dataPath) # check it now to avoid lost time

        # - - - - - - - - - data containers - - - - - - - -
                        
        # data divided by dom=train/val , role, 
        self.amino_data={} # [dom][role][[yaml-list-as-is]]

        # 1-hot encoded data ready for training
        self.data={} # [dom][XA[],XB[],Y[],AUX[]]
        
        # prepare 1-hot base for amino:
        myDim=len(self.basesL)
        self.oneHotBase={} # len=1+myDim, pad=00000000...
        i=0
        for x in self.basesL:
            self.oneHotBase[x]=np.eye(myDim)[i]
            i+=1
        self.oneHotBase[self.padChar]=np.zeros(myDim)
        print('oneHot base, size=',len(self.oneHotBase), ', sample:')
        for x in list( self.oneHotBase.keys()):
            print('base:',x,'1-hot:',self.oneHotBase[x].tolist())
        
        print('all bases :', list( self.oneHotBase.keys()))
        print('use seqLenCut=',self.seqLenCut)

        #print('Cnstr ready:%s\n'%self.name)
 
   #............................
    def load_one_scaffold_fasta(self,fName):
        #print('read ',fName)
        fp = open(fName, 'r')
        out=[]
        for line in fp:
            if '>'==line[0] : continue
            #print('ll=%s='%line)
            line=line[:-1]
            setACTG=set(line)
            #print(fName,setACTG)
            assert self.basesSet.issuperset(setACTG)
            out.append(line)
        fp.close()
        return  ''.join(out)

    #............................
    def encode1hotBase(self,seqPad): # convert sequences to 1-hot 2-D arrays
        hot2D=[]
        for y in seqPad:
            hot2D.append(self.oneHotBase[y])
        return np.array(hot2D).astype(np.float32)

    #............................
    def add_prot_to_1hot(self,XhotAll,XfloatAll,AuxAll,ieve,oneL):
        protN,seqLen,fastaIdx,seqStr0=oneL

        # prep amino acid sequence ...
        seq_len=self.seqLenCut
        strTag='none' # just for counting
        # unify seqence length
        dm=seq_len - len(seqStr0)
        if dm<=0: # clip if too long
            seqStr=seqStr0[:seq_len]
            strTag='long'
        else:  # prepend padding
            seqStr=seqStr0+self.padChar*dm
            strTag='short'
        #print('wq1',ieve,XhotAll.shape,XfloatAll.shape,len(seqStr),len(Xfloat))
        XhotOne=self.encode1hotBase(seqStr)
        XhotAll[ieve,:]=XhotOne[:]
        
        # prep glob floats
        floatL=[(np.log(seqLen)-4)/4.]  # heuristic, keeps value [0,1]
        XfloatAll[ieve,:]=floatL[:]

        # prep Aux info
        AuxAll[ieve]={'fastaIdx':fastaIdx,'protN':protN}
        return strTag
 
    #............................
    def build_data_one(self,oneL):

        num0=1 # just one protein
        seq_len=self.seqLenCut        
        num_bases=len(self.basesL)
        num_features=len(self.globFeatureL)

        # clever list-->numpy conversion
        XhotAll=np.zeros([num0,seq_len,num_bases],dtype=np.float32)
        XfloatAll=np.zeros([num0,num_features],dtype=np.float32)
        AuxAll=['aa' for x in range(num0)  ]   # put here aux variables as list
        strTag=self.add_prot_to_1hot(XhotAll,XfloatAll,AuxAll,0,oneL)
               
        
        return XhotAll,XfloatAll,AuxAll,strTag

    #............................
    def classify_one_protein(self,Xs,Xf,verb=1):
        
        nK=len(self.model)
        #print('clpr  inputs shapes:',Xs.shape,Xf.shape,' using kModel=%d models'%(nK))
        assert Xs.shape[0]==1 # only one protein at a time
        start = time.time()

        Yscore=[]
        
        for k in self.model:
            Yscore.append(self.model[k].predict([Xs,Xf]).flatten())
            if verb>1: print(k,' kModel done, score=',Yscore[-1])
        if verb: print('   prediction  done,  elaT=%.1f sec'%((time.time() - start)))

        Yscore=np.array(Yscore)
        avr=float(Yscore.mean())
        std=float(Yscore.std())
        err=std
        if Yscore.shape[0]>1 : err=float(std/np.sqrt(Yscore.shape[0]-1))
        avr_str="%.3f +/- %.3f"%(avr,std)

        rec={'avr':avr,'std':std,'err':err}
        if verb >0: print(rec)
        return avr_str,rec


    #............................
    def load_Kmodels(self,path='.',kL=['']):
        # expand list if '-' are present
        kkL=[]
        for x in kL:
            if '-' not in x:
                kkL.append(x) ; continue
            xL=x.split('-')
            for i in range(int(xL[0]),int(xL[1])+1):
                kkL.append(i)
        #print(kL,'  to ',kkL)
        kL=kkL

        nK=len(kL)
        assert nK>0
        self.model={}
        runDateStr=self.runDate.strftime("%Y-%m-%d_%H.%M")
        self.info={'modelPath':path,'kModelList':kL,'oracle':self.name,'runDate':runDateStr}

        print('load_kModels =',nK,kL)
        start = time.time()
        for k in kL:
            inpF5m=path+'%s/'%k+self.name+'.model.h5'
            print('load %d model and weights  from'%len(self.model),inpF5m,'  ... ')
            self.model[k]=load_model(inpF5m) # creates mode from HDF5
            print('loaded k=',k)
            if len(self.model)==1 :
                self.model[k].summary()
            else:
                assert self.model[k].count_params() == self.model[kL[0]].count_params()
        print('%d models loaded, elaT=%.1f sec'%(nK,(time.time() - start)))
