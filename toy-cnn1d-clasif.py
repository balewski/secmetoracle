#!/usr/bin/env python

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
#1os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
#1warnings.filterwarnings("ignore") #Hide messy Numpy warnings


startT0 = time.time()
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Reshape,Conv1D,Flatten,MaxPool1D,concatenate,Dropout
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LeakyReLU # ??breaks model_save unless it is a separate layer
print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))
assert tf.__version__ >='1.12.0'
#-------------------
def build_model(X ): # this has big input
    XA,XB=X
    shA=XA.shape
    shB=XB.shape
    xA = Input(shape=(shA[1],shA[2]), name='seq_%d_x_%d'%(shA[1],shA[2]))
    xB = Input(shape=(shB[1],), name='globF_%d'%(shB[1]))
    print('build_model inpA:',xA.get_shape(),'  inpB:',xB.get_shape())

    # CNN params
    strides=1
    kernel = 5
    pool_len = 3 # how much time_bins get reduced per pooling
    dropFrac=0.2
    cnnDim=[4,6]
    fcDim=[20,10]

    numConv_lr=len(cnnDim)
    numFC_lr=len(fcDim)
    
    hiddenAct='leakyReLU'
    #hiddenAct='relu'

    print('numConv',numConv_lr,'numFC',numFC_lr,' hiddenAct=',hiddenAct)

    hidAct=hiddenAct # clumsy way to insert LeakyReLU if requested
    if hiddenAct=='leakyReLU':
        hidAct='linear'
        print('insert',hiddenAct)
        
    h=xA

    # .....  CNN-1D layers: OFF ,activation=hidAct
    for i in range(numConv_lr):
        dim=cnnDim[i]
        print('ii',i,dim,kernel,strides)
        h= Conv1D(dim,kernel,strides=strides, padding='same',name='cnb%d_d%d_k%d'%(i,dim,kernel))(h)
        if hiddenAct=='leakyReLU':
            h =LeakyReLU(name='acb%d'%(i))(h)
        h= MaxPool1D(pool_size=pool_len, name='pool_%d'%(i))(h)
        h = Dropout(dropFrac,name='cnndrop%d'%i)(h)


    netA=Flatten(name='to_1d')(h)
    h = concatenate([netA,xB],name='add_AB')

    # .... FC  layers   
    for i in range(numFC_lr):
        dim = fcDim[i]
        h = Dense(dim,activation=hidAct,name='enc%d'%i)(h)
        if hiddenAct=='leakyReLU':
            h =LeakyReLU(name='acc%d'%(i))(h)
        if i < numFC_lr-1:
            h = Dropout(dropFrac,name='fcdrop%d'%i)(h)

    out_dim=1 # this is binary classfire
    y= Dense(out_dim, activation='sigmoid',name='out_%d'%out_dim)(h)

    #print('build_model: loss=',self.lossName,' optName=',self.optimizerName,' out:',y.get_shape())
    # full model
    model = Model(inputs=[xA,xB], outputs=y)

    myLoss='binary_crossentropy'
    model.compile(loss = myLoss,optimizer = 'adam')

    model.summary() # will print

    print('\nFull  loss=%s layers=%d , params=%.1f K'%(myLoss,len(model.layers),model.count_params()/1000.))

    

    '''
    filterA = [2, 4]
    kernelA = [3, 7]
    strides=2

    assert len(filterA)==len(kernelA)
    numFan=len(filterA)

    input_shape=(time_dim,)
    x_input = Input(shape=input_shape, name='inp_x')
    print('build_model X_inp:',x_input.get_shape())
    x_3d = Reshape((-1, 1), name='pass0') (x_input) # because Conv1D wants rank 3 tensor 
    print('resh0=',x_3d.get_shape())    

    fan_outL = []
    for i in range(numFan):
        pool_len=1+kernelA[i]
        conv_lr = Conv1D(filterA[i],kernelA[i],strides=strides, padding='same',name='fan%d_f%d_k%d'%(i,filterA[i],kernelA[i]))(x_3d)
        act_lr =LeakyReLU(name='act%d'%(i))(conv_lr)
        pool_lr = MaxPool1D(pool_size=pool_len)(act_lr)
        fan_outL.append( Flatten()(pool_lr))

    #merged_conv_outputs =  concatenate(fan_outL, name="concat_%d"%len(filterA))
    merged_conv_outputs = fan_outL[0]
    print('merg=',merged_conv_outputs.get_shape(),' out_dim=',out_dim)    
 
    softmaxL = Dense(out_dim, activation="softmax")(merged_conv_outputs)
    #softmaxL = Dense(out_dim, activation="softmax")( Flatten()(pool_lr))
    model = Model(input=x_input, output=softmaxL)
    '''
    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================

seqLenCut=700
num_bases=21
num_globF=2

print('\ngenerate data and train')
num_eve=1000
XA=np.random.normal( size=(num_eve,seqLenCut,num_bases))
XB=np.random.normal( size=(num_eve,num_globF))
X=[XA,XB]
print('XA shape:',XA.shape,' XB shape:',XB.shape)
model=build_model(X)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outG='cnn1d-AE.graph.svg'
    plot_model(model, to_file=outG, show_shapes=True, show_layer_names=True)  # works only on Cori
    print('saved model graph as ',outG)


outM='cnn1d-AE.model.h5'
model.save(outM)
print('saved h5 model as ',outM)

print('\ngenerate data and train')
Y=np.random.normal( size=(num_eve,1))
model.fit(X,Y, batch_size=100, epochs=2,verbose=1)
print('fit done, predicting')
Xnew=model.predict(X)
print('done, Xnew:',Xnew.shape)

from tensorflow.keras.models import load_model
model2=load_model(outM)
print('load h5 model h5 from ',outM)
