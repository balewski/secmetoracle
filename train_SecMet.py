#!/usr/bin/env python
""" read input 
train net
write net + weights as HD5

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_SecMet import Plotter_SecMet
from Deep_SecMet import Deep_SecMet

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2,3],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',
                         default='hset1',
                         help=" hyper-param set, aka design of the model")
    parser.add_argument("--dataPath", default='data',
                        help="path to input")
    parser.add_argument("--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("--seedModel",
                        default=None,
                        help="seed model and weights")
 
    parser.add_argument("--seedWeights",
                        default=None,
                        help="seed weights only, after model is created, for re-trainig only ")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="num epochs")

    parser.add_argument("-b", "--batch_size", type=int, default=128,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=9000,
                        help="num seqences for training")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")

    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=30,
                         help="early stop:  # epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLr", dest='reduceLearnPatince', type=int, default=0,
                        help="reduce learning at plateau after N epochs, 0=off")


    args = parser.parse_args()
    args.prjVersion='1'
    args.prjName='secmet'+args.prjVersion # core name used everywhere

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_SecMet(args)
gra=Plotter_SecMet(args )

for role in deep.roleL:
    deep.prep_labeled_input(role,'val') 
    deep.prep_labeled_input(role,'train')
 
for dom in deep.amino_data:
    deep.build_training_data(dom)
    continue
    gra.plot_float_featuresB(deep,dom,0,20)
    gra.plot_float_featuresB(deep,dom,1,21)

if args.seedModel==None:
    print('start fresh training')
    deep.build_compile_model(args)
else:
    deep.load_compile_1model(path=args.seedModel)

gra.plot_model(deep,2) # depth:0,1,2

if args.seedWeights:
    deep.load_weights_4_retrain(path=args.seedWeights)

if args.epochs >3:  deep.save_model() 

if args.epochs >0:
    deep.train_model(args) 
    deep.save_model() 
    deep.save_training_history() 
    gra.plot_train_history(deep,args,10)
    y_true, y_pred=gra.plot_AUC('val',deep,args,10)
else:
    y_true, y_pred=gra.plot_AUC('val',deep,args)

gra.plot_labeled_scores(y_true, y_pred,deep.roleL,'seg=val')
gra.plot_confusion_matrix(y_true, y_pred,deep.roleL,'seg=val')

gra.display_all(args,'train')
print('training has been completed')





